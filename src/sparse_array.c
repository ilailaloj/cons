#include "sparse_array.h"

#include "cons_debug.h"
#include "common.h"
#include "linked_list.h"
#include "stdio.h"
#include <stdbool.h>
#include <threads.h>

sarray_t *sarray_init( void )
{
    __begin sarray_t *arr = calloc( 1, sizeof( sarray_t ) );
    if ( arr == NULL )
        goto error;
    
    __success return arr;

error:
    __failure return NULL;
}

void __sarray_reset( sarray_t *arr )
{
    arr->root = NULL;
}

bool sarray_empty( const sarray_t *arr )
{
    __success return arr->root == NULL;
}

sarray_node_t *
__sarray_find_free_spot( const sarray_node_t *root, 
                         size_t index )
{
    __begin;
    
    sarray_node_t *n = ( sarray_node_t * ) root;
    
    while ( n->index != index )
    {
        if ( n->index > index )
        {
            if ( n->lhs != NULL )
            {
                n = n->lhs;   
                continue;
            }
        }

        if ( n->index < index )
        {
            if ( n->rhs != NULL )
            {
                n = n->rhs;
                continue;
            }
        }

        break;
    }

    __success return n;
}

sarray_node_t *
__sarray_mknode( sarray_node_t *parent, size_t  index, void* data )
{
    __begin;
    sarray_node_t *n = 
        calloc( 1, sizeof( sarray_node_t ) );
    
    if ( n == NULL )
        goto err; 

    n->index = index;
    n->data  = data;
    n->par   = parent;

    if ( parent == NULL )
        goto end;

    if ( parent->index > n->index )
        parent->lhs = n;
    else if ( parent->index < n->index )
        parent->rhs = n;

end:
    __success return n;

err:
    __failure return NULL;
}

inline const sarray_node_t *
__sarray_get_least_index_node( const sarray_t *arr )
{
    __begin;
    
    const sarray_node_t *n = arr->root;
    while ( n->lhs != NULL )
    {
        n = n->lhs;
    }

    __success return n;
}

const void *sarray_get( const sarray_t *arr, size_t index )
{
    __begin;
    if ( sarray_empty( arr ) )
        goto not_found;

    const sarray_node_t *n = 
        __sarray_find_free_spot( arr->root, index );

    if ( n->index != index )
        goto not_found;

    __success return n->data;

not_found:
    __failure return NULL;
}

int __sarray_add_node( sarray_t *arr, sarray_node_t *node )
{
    __begin;

    if ( sarray_empty( arr ) )
    {
        arr->root = node;
        goto end;
    }

    sarray_node_t *free_spot = __sarray_find_free_spot( arr->root, node->index );

    if ( free_spot->index == node->index )
        goto err;

    if ( free_spot->lhs || free_spot->rhs )
        goto err;

    if ( !free_spot->lhs && free_spot->index > node->index )
        free_spot->lhs = node;
    
    if ( !free_spot->rhs && free_spot->index < node->index )
        free_spot->rhs = node;

end:
    return OK;

err:
    return ERR;
}

int sarray_set( sarray_t *arr, void *data, size_t index )
{
    __begin;

    if ( sarray_empty( arr ) )
    {
        arr->root = __sarray_mknode( NULL, index, data );

        if ( !arr->root )
            goto err;

        goto end;
    }

    sarray_node_t *free_spot = __sarray_find_free_spot( arr->root, index );

    if ( free_spot->index != index ) 
    {
        const sarray_node_t *node = 
            __sarray_mknode( free_spot, index, data );

        if ( !node )
            goto err;

        goto end;
    }

    free_spot->data = data;

end:
    __success return OK;

err:
    __failure return ERR;
}

void __sarray_get_iter( sarray_node_t *node, 
                        sarray_iter_t *iter )
{
    __begin;
    
    if ( node->lhs )
        __sarray_get_iter( node->lhs, iter );
    
    llist_add_tail( iter, node );
    
    if ( node->rhs )
        __sarray_get_iter( node->rhs, iter);

    __success;
}

sarray_iter_t *sarray_get_iter( const sarray_t *arr )
{
    __begin sarray_iter_t *iter = llist_init();
    __sarray_get_iter( arr->root, iter );
    __success return iter;
}

void sarray_iter_destroy( sarray_iter_t *iter )
{
    __success llist_destroy( iter );
}

void __sarray_reset_node( sarray_node_t *node )
{
    node->lhs = NULL;
    node->rhs = NULL;
}

void __sarray_destroy( sarray_node_t *root)
{
    if ( !root )
        return;
    
    if ( !root->lhs && !root->rhs )
        goto end;

    __sarray_destroy( root->lhs );
    __sarray_destroy( root->rhs );

end:
    __sarray_reset_node( root );
    free( root );
}

void sarray_destroy( sarray_t *arr )
{
    __sarray_destroy( arr->root );
    free( arr );
}

void __sarray_remove_iteration( sarray_t *arr,
                                sarray_node_t *node )
{
    __sarray_add_node( arr, node );

    if ( node->lhs )
        __sarray_remove_iteration( arr, node->lhs );
    if ( node->rhs )
        __sarray_remove_iteration( arr, node->rhs );
}

int sarray_remove( sarray_t *arr, size_t index )
{
    __begin;

    if ( sarray_empty( arr ) )
        goto end;

    sarray_node_t *node = __sarray_find_free_spot( arr->root, index );
    if ( node == NULL || node->index != index )
        goto err;

    if ( node != arr->root )
    {
        sarray_node_t *parent = node->par;
        if ( parent == NULL || ( parent->lhs != node && parent->rhs != node ) )
            goto err;
        
        parent->lhs = ( parent->lhs == node ) ? NULL : parent->lhs;
        parent->rhs = ( parent->rhs == node ) ? NULL : parent->rhs;
    }
    else
    {
        arr->root = NULL;
    }

    if ( node->lhs )
        __sarray_remove_iteration( arr, node->lhs );
    if ( node->rhs )
        __sarray_remove_iteration( arr, node->rhs );

    free( node );

end:
    __success return OK;

err:
    __failure return ERR;
}

#define __get_node( _iter_node ) \
    ( ptr_cast( sarray_node_t, _iter_node->data ) )

bool __sarray_eq( const sarray_iter_node_t *lhs,
                  const sarray_iter_node_t *rhs )
{
    if ( !lhs && !rhs )
        return true;

    if ( !lhs || !rhs )
        return false;

    if ( __get_node( lhs )->data != __get_node( rhs )->data )
        return false;
        
    return __sarray_eq( lhs->next, rhs->next );
}

#undef get_node

bool sarray_eq( const sarray_t *lhs, const sarray_t *rhs )
{
    sarray_iter_t *liter = sarray_get_iter( lhs );
    sarray_iter_t *riter = sarray_get_iter( rhs );
    
    bool ret = __sarray_eq( liter->head, riter->head );
    
    llist_destroy( liter );
    llist_destroy( riter );

    return ret;
}