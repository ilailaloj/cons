/// @file linked_list.h
/// @brief Simplest implementation of doubly linked list
/// @copyright Copyright 2024 Anufriev Ilia (anufriewwi@rambler.ru)

#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

#include "stdbool.h"

typedef struct linked_list_node {
    void *data;
    struct linked_list_node *next;
    struct linked_list_node *prev;
} llist_node_t;

typedef struct linked_list {
    struct linked_list_node *head;
    struct linked_list_node *tail;
} llist_t;


/// @brief  initialize an empty list. Needs to be deallocated via llist_destroy.
/// @return initialized llist_t struct
llist_t *llist_init( void );

/// @brief  check whether the llist is empty
/// @param  list - list to be checked
bool llist_empty( const llist_t *list );

/// @brief  check whether two llists are equal. 
/// @param  lhs, rhs - lists to be compared.
bool llist_eq( const llist_t *lhs, const llist_t *rhs );

/// @brief  add a new node to the head of the list.
/// @param  list - list to which data will be added.
/// @param  data - pointer to the data to be added.
/// @return OK if successful. 
int llist_add_head( llist_t *list, void *data );

/// @brief  add a new node to the tail of the list.
/// @param  list - list to which data will be added.
/// @param  data - pointer to the data to be added.
/// @return OK if successful.
int llist_add_tail( llist_t *list, void *data );

/// @brief  removes and deallocates node from the tail of the list.
/// @param  list - a list that will have its tail popped.
/// @note   data is deallocated separately.
void llist_pop_tail( llist_t *list );

/// @brief  removes and deallocates a node from the head of the list.
/// @param  list - a list that will have its head popped.
/// @note   data is deallocated separately.
void llist_pop_head( llist_t *list );

/// @brief  removes a node from within the list.
/// @param  list - a list that will have a node removed
/// @param  node - a node that will be removed and deallocated.
/// @note   data is deallocated separately.
int llist_remove_node( llist_t *list, llist_node_t *node );

/// @brief  deallocate the list header and all its nodes
void llist_destroy( llist_t *list );

/// @brief  advance a list node pointer forwards
#define llist_advance( node ) \
    node = node->next

/// @brief  advance a list node pointer forwards N times
#define llist_nadvance( node, n ) \
    for ( int i = 0; i < n; ++i ) \
        node = node->next

/// @brief  advance a list node pointer backwards
#define llist_back( node ) \
    node = node->prev

/// @brief  advance a list node pointer backwards N times 
#define llist_nback( node, n )    \
    for ( int i = 0; i < n; ++i ) \
        node = node->prev

#define for_each_in_llist( _llist, _iter ) \
    for ( llist_node_t *_iter = _llist->head; _iter; llist_advance( _iter ) )

#define for_each_in_llist_const( _llist, _iter ) \
    for ( const llist_node_t *_iter = _llist->head; _iter; llist_advance( _iter ) )

#endif // _LINKED_LIST_H_