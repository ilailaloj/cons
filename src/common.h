#ifndef _CONS_COMMON_H_
#define _CONS_COMMON_H_

#include "cons_debug.h"

#define OK   0
#define ERR -1

#define ptr_cast( type, ptr ) \
    ( ( type * ) ptr )

#define ptr_deref( type, ptr ) \
    ( *ptr_cast( type, ptr ) )

#endif