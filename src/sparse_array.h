/// @file sparse_array.h
/// @copyright Copyright 2024 Anufriev Ilia (anufriewwi@rambler.ru)
#ifndef _SPARSE_ARRAY_H_
#define _SPARSE_ARRAY_H_

#include "stdlib.h"
#include "stdbool.h"
#include "linked_list.h"

#define OK   0
#define ERR -1

typedef struct _sarray_node {
    void *data;
    size_t index;
    struct _sarray_node *lhs;
    struct _sarray_node *rhs;
    struct _sarray_node *par;
} sarray_node_t;

typedef struct _sarray {
    struct _sarray_node *root;
} sarray_t;

/// @brief  allocate memory for the tree and return pointer to it
/// @param  data - data to be put into the root node
sarray_t *sarray_init( void );

/// @brief  adds data to the sparse array.
/// @param  arr - sparse array
/// @param  data - data to be put into the sparse array
/// @param  index - index at which the data will be inserted
/// @return status. 0 on success, negative error code on error
int sarray_set( sarray_t *arr, void *data, size_t index );

/// @brief  gets value by index
/// @param  arr - sparse array
/// @param  index - an index of the value
/// @return void pointer to the value stored inside. NULL if index was not found
const void *sarray_get( const sarray_t *arr, size_t index );


/// @brief  macro simplifying the process of getting a pointer of a needed type
#define sarray_get_type( type, arr, index ) \
    ( ( const type * ) sarray_get( arr, index ) )

/// @brief  removes value from sparce array 
/// @param  arr - sparce array
/// @param  index - index of the node to be freed.
/// @return 0 on success, negative value if an element was not found. 
int sarray_remove( sarray_t *arr, size_t index );

/// @brief  frees resources allocated for the tree structure. It does not free
///         data.
void sarray_destroy( sarray_t *arr );

bool sarray_empty( const sarray_t *arr );

bool sarray_eq( const sarray_t *lhs, const sarray_t *rhs );

typedef llist_t sarray_iter_t;
typedef llist_node_t sarray_iter_node_t;

sarray_iter_t *sarray_get_iter( const sarray_t *arr );

void sarray_iter_destroy( sarray_iter_t *iter );

/// @brief  iterating over the sarray.
/// @param  _sarray - an array to iterate over
/// @param  _iter - name of the iterator variable
#define for_each_in_sarray_begin( _sarray, _iter )          \
    {   sarray_iter_t *_iter##_header = sarray_get_iter( _sarray ); \
        sarray_iter_node_t *_iter = _iter##_header->head;     \
        for ( ; _iter; llist_advance( _iter ) )

/// @brief  deallocates iterator created in the _begin section of the loop 
#define for_each_in_sarray_end( _iter ) \
        sarray_iter_destroy( _iter##_header ); }

/// @brief  iterating over the sarray.
/// @param  _sarray - an array to iterate over
/// @param  _iter - name of the iterator variable
#define for_each_in_sarray_const_begin( _sarray, _iter )          \
    {   sarray_iter_t *_iter##_header = sarray_get_iter( _sarray ); \
        const sarray_iter_node_t *_iter = _iter##_header->head;     \
        for ( ; _iter; llist_advance( _iter ) )

/// @brief  deallocates iterator created in the _begin section of the loop 
#define for_each_in_sarray_const_end( _iter ) \
        for_each_in_sarray_end( _iter##_header )

/// @brief  iterates over the sarray using the preallocated _iter variable.
/// @param  _sarray - sarray to iterate over
/// @param  _iter - sarray_iter_node_t variable to be used as iterator.
#define for_each_in_sarray( _sarray, _iter ) \
        for ( ; _iter; llist_advance( _iter ) )

/// @brief  iterates over the sarray using the preallocated _iter variable.
///         using const pointers.
/// @param  _sarray - sarray to iterate over
/// @param  _iter - sarray_iter_node_t variable to be used as iterator.
#define for_each_in_sarray_const( _sarray, _iter ) \
        for ( ; _iter; llist_advance( _iter ) )

/// @brief  extract sarray_node_t from the sarray_iter_node_t.
#define sarray_iter_get_node( _iter_node ) \
        ( ptr_cast( sarray_node_t, _iter_node->data ) )
#endif // _SPARSE_ARRAY_H_