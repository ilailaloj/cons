#include <complex.h>
#include "cons_debug.h"
#include "stdlib.h"
#include "linked_list.h"
#include "common.h"

#define advance( node ) \
    node = node->next;

llist_t *llist_init( void )
{
    __success return calloc( 1, sizeof( llist_t ) );    
}

bool llist_empty( const llist_t *list ) 
{
    __success return list->head == NULL && list->tail == NULL;
}

void __llist_reset( llist_t *list )
{
    list->head = NULL;
    list->tail = NULL; 
}

int llist_add_head( llist_t *list, void *data ) 
{
    __begin;
    
    llist_node_t *prev = calloc( 1, sizeof( llist_node_t ) );

    if ( !prev )
        goto err;

    if ( !llist_empty( list ) )
    {
        list->head->prev = prev;
        prev->next = list->head;
    }
    else 
    {
        list->tail = prev;
    }

    list->head = prev;
    prev->data = data;

    __success return OK;

err:
    __failure return ERR;
}

int llist_add_tail( llist_t *list, void *data )
{
    __begin;

    llist_node_t *next = calloc( 1, sizeof( llist_node_t ) );

    if ( !next )
        goto err;

    if ( !llist_empty( list ) )
    {
        list->tail->next = next;
        next->prev = list->tail;
    }
    else 
    {
        list->head = next;
    }

    list->tail = next;
    next->data = data;       

    __success return OK;

err:
    __failure return ERR;
}

void llist_pop_tail( llist_t *list )
{
    __begin;

    llist_node_t *new_tail = list->tail->prev;
    free( list->tail );

    list->tail = new_tail;

    if ( list->tail )
        list->tail->next = NULL;
    else
        __llist_reset( list );

    __success;
}

void llist_pop_head( llist_t *list )
{
    __begin;

    llist_node_t *new_head = list->head->next;
    free( list->head );

    list->head = new_head;
    
    if ( list->head )
        list->head->prev = NULL;
    else
        __llist_reset( list );

    __success;
}

int llist_remove_node( llist_t *list, llist_node_t *node )
{   
    __begin;

    if ( !list || !node )
        goto err;
    
    if ( node == list->tail )
    {
        llist_pop_tail( list );
        goto end;
    }

    if ( node == list->head )
    {
        llist_pop_head( list );
        goto end;
    }
    
    for ( llist_node_t *iter = list->head; iter != NULL; iter = iter->next  )
    {
        if ( iter == node )
        {
            llist_node_t *next = node->next;
            llist_node_t *prev = node->prev;

            if ( next )
                next->prev = prev;
            
            if ( prev )
                prev->next = next;
            
            break;
        }
    }

    free( node );
end:
    __success return OK;

err:
    __failure return ERR;
}

void llist_destroy( llist_t *list )
{
    __begin;
    if ( !list )
        goto end;

    for ( llist_node_t *iter = list->head; iter; )
    {
        llist_node_t *tbd = iter;
        iter = iter->next;
        free( tbd );
    }

    free( list );
end:
    __success;
}

bool __llist_eq( const llist_node_t *lhs, const llist_node_t *rhs )
{
    if ( !lhs && !rhs )
        return true;
    
    if ( !lhs || !rhs )
        return false;
    
    if ( lhs->data != rhs->data )
        return false;

    return __llist_eq( lhs->next, rhs->next );
}

bool llist_eq( const llist_t *lhs, const llist_t *rhs )
{
    return __llist_eq( lhs->head, rhs->head );
}