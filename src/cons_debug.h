#ifndef _CONS_DEBUG_H_
#define _CONS_DEBUG_H_

#define DEBUG 9 
#define DEBUG_STR "[ DEBUG ]: "
#define TEST_STR  "[ TEST ]: "

#include "stdio.h"

enum debug_level {
    nodebug  = 0,
    print    = 5,
    full     = 10,
};

#define PRINT_STMT( statement, fmt ) \
    fprintf( stderr, DEBUG_STR "From %s: " #statement " = " fmt ".\n", __func__, statement ); \
    fflush( stderr );

#define PRINTLN( statement ) \
    fprintf( stderr, DEBUG_STR "From %s: %s\n", __func__, statement ); \
    fflush( stderr );

#define __begin \
    if ( DEBUG >= full ) fprintf( stderr, DEBUG_STR "%s started.\n", __func__ );

#define __failure \
    if ( DEBUG >= full ) fprintf( stderr, DEBUG_STR "%s failed.\n", __func__ );

#define __success \
    if ( DEBUG >= full ) fprintf( stderr, DEBUG_STR "%s returned.\n", __func__ );

#endif // _CONS_DEBUG_H_