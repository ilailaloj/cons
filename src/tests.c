#include "common.h"
#include "cons_debug.h"
#include "linked_list.h"
#include "assert.h"
#include "sparse_array.h"

int test_data[] = { 0, 1, 2, 3, 4 };

#define RUN_TEST( test )                     \
    /*printf( DEBUG_STR #test " STARTED!\n" );*/ \
    test();                                  \
    printf( TEST_STR #test " OK!\n" );

void test_llist_empty()
{
    llist_t *l1 = llist_init();

    assert( llist_empty( l1 ) );

    llist_add_head( l1 , test_data + 0 );
    
    assert( l1->head == l1->tail );

    llist_add_head( l1, test_data + 1 );

    assert( l1->head != l1->tail );

    llist_remove_node( l1, l1->head->next );
    llist_remove_node( l1, l1->head );

    assert( llist_empty( l1 ) );
    
    llist_destroy( l1 );
}

void test_llist_simple_cmp()
{
    llist_t *l1 = llist_init();

    llist_add_head( l1, test_data + 0 );
    llist_add_head( l1, test_data + 1 );
    llist_add_head( l1, test_data + 2 );
    llist_add_head( l1, test_data + 3 );

    llist_t *l2 = llist_init();

    llist_add_head( l2, test_data + 0 );
    llist_add_head( l2, test_data + 1 );
    llist_add_head( l2, test_data + 2 );
    llist_add_head( l2, test_data + 3 );

    assert( llist_eq( l1, l2 ) );

    llist_t *l1p = l1;

    assert( llist_eq( l1, l1p ) );

    llist_destroy( l1 );
    llist_destroy( l2 );
}

void test_llist_remove()
{
    llist_t *l1 = llist_init();

    llist_add_head( l1, test_data + 0 );
    llist_add_head( l1, test_data + 1 );
    llist_add_head( l1, test_data + 2 );
    llist_add_head( l1, test_data + 3 );

    llist_t *l2 = llist_init();

    llist_add_head( l2, test_data + 0 );
    llist_add_head( l2, test_data + 2 );
    llist_add_head( l2, test_data + 3 );

    llist_node_t *l1node = l1->head;
    llist_nadvance( l1node, 2 );

    llist_remove_node( l1, l1node );
    
    assert( llist_eq( l1, l2 ) );

    llist_destroy( l1 );
    llist_destroy( l2 );

    l1 = llist_init();

    llist_add_head( l1, test_data + 0 );
    llist_add_head( l1, test_data + 1 );
    llist_add_head( l1, test_data + 2 );
    llist_add_head( l1, test_data + 3 );

    l2 = llist_init();

    llist_add_head( l2, test_data + 0 );
    llist_add_head( l2, test_data + 1 );
    llist_add_head( l2, test_data + 2 );

    const llist_node_t *l1_nhead = l1->head->next;

    llist_remove_node( l1, l1->head );

    assert( l1->head == l1_nhead );

    const llist_node_t *l1_ntail = l1->tail->prev;

    llist_remove_node( l1, l1->tail );

    assert( l1->tail == l1_ntail );

    llist_destroy( l1 );
    llist_destroy( l2 );
}

void test_llist_last_element()
{
    llist_t *l1 = llist_init();

    llist_add_tail( l1, test_data + 1 );
    llist_add_tail( l1, test_data + 2 );

    assert( l1->head != l1->tail );

    llist_remove_node( l1, l1->head );

    assert( l1->head == l1->tail );

    llist_node_t *l1node = l1->head; 

    llist_remove_node( l1, l1node );

    assert( !l1->head );
    assert( !l1->tail );

    llist_destroy( l1 );
}

void test_llist_pop_head()
{
    llist_t *l1 = llist_init();

    llist_add_head( l1, test_data + 0 );
    llist_add_head( l1, test_data + 1 );
    llist_add_head( l1, test_data + 2 );
    llist_add_head( l1, test_data + 3 );

    llist_t *l2 = llist_init();

    llist_add_head( l2, test_data + 0 );
    llist_add_head( l2, test_data + 1 );
    llist_add_head( l2, test_data + 2 );

    const llist_node_t *l1nhead = l1->head->next;
    llist_remove_node( l1, l1->head );

    assert( l1->head == l1nhead );
    assert( llist_eq( l1, l2 ) );

    llist_destroy( l1 );
    llist_destroy( l2 );
}

void test_llist_pop_tail()
{
    llist_t *l1 = llist_init();

    llist_add_tail( l1, test_data + 0 );
    llist_add_tail( l1, test_data + 1 );
    llist_add_tail( l1, test_data + 2 );
    llist_add_tail( l1, test_data + 3 );

    llist_t *l2 = llist_init();

    llist_add_tail( l2, test_data + 0 );
    llist_add_tail( l2, test_data + 1 );
    llist_add_tail( l2, test_data + 2 );

    const llist_node_t *l1nhead = l1->head;
    llist_pop_tail( l1 );

    assert( l1->head == l1nhead );
    assert( llist_eq( l1, l2 ) );

    llist_destroy( l1 );
    llist_destroy( l2 );
}

void test_llist()
{
    RUN_TEST( test_llist_simple_cmp );
    RUN_TEST( test_llist_remove );
    RUN_TEST( test_llist_last_element );
    RUN_TEST( test_llist_pop_head );
    RUN_TEST( test_llist_pop_tail );
    RUN_TEST( test_llist_empty );
}

void test_sarray_simple_cmp()
{
    sarray_t *sa1 = sarray_init();

    sarray_set( sa1, test_data + 0, 1 );
    sarray_set( sa1, test_data + 1, 3 );
    sarray_set( sa1, test_data + 2, 4 );
    sarray_set( sa1, test_data + 3, 2 );

    sarray_t *sa2 = sarray_init();

    sarray_set( sa2, test_data + 2, 4 );
    sarray_set( sa2, test_data + 0, 1 );
    sarray_set( sa2, test_data + 1, 3 );
    sarray_set( sa2, test_data + 3, 2 );

    assert( sarray_eq( sa1, sa2 ) );
    sarray_destroy( sa1 );
    sarray_destroy( sa2 );
}

void test_sarray_remove_node()
{
    sarray_t *sa1 = sarray_init();

    sarray_set( sa1, test_data + 0, 1 );
    sarray_set( sa1, test_data + 1, 3 );
    sarray_set( sa1, test_data + 2, 4 );
    sarray_set( sa1, test_data + 3, 2 );

    sarray_t *sa2 = sarray_init();

    sarray_set( sa2, test_data + 2, 4 );
    sarray_set( sa2, test_data + 1, 3 );
    sarray_set( sa2, test_data + 3, 2 );

    sarray_remove( sa1, 1 );

    assert( sarray_eq( sa1, sa2 ) );

    sarray_destroy( sa1 );
    sarray_destroy( sa2 );
}

void test_sarray_remove_root()
{
    sarray_t *sa1 = sarray_init();

    sarray_set( sa1, test_data + 0, 1 );
    sarray_set( sa1, test_data + 1, 3 );
    sarray_set( sa1, test_data + 3, 2 );

    sarray_remove( sa1, 1 );

    sarray_t *sa2 = sarray_init();

    sarray_set( sa2, test_data + 1, 3 );
    sarray_set( sa2, test_data + 3, 2 );

    sarray_eq( sa1, sa2 );

    sarray_destroy( sa1 );
    sarray_destroy( sa2 );
}


void test_sarray_remove_last_node()
{
    sarray_t *sa1 = sarray_init();

    sarray_set( sa1, test_data + 3, 2 );

    sarray_remove( sa1, 2 );

    assert( sarray_empty( sa1 ) );

    sarray_destroy( sa1 );
}

void test_sarray_set_get()
{
    sarray_t *sa1 = sarray_init();
    
    sarray_set( sa1, test_data + 3, 2 );

    assert( *sarray_get_type( int, sa1, 2 ) == *( test_data + 3 ) );

    sarray_set( sa1, test_data + 2, 1 );

    assert( *sarray_get_type( int, sa1, 1 ) == *( test_data + 2 ) );
    
    sarray_set( sa1, test_data + 1, 3 );

    assert( *sarray_get_type( int, sa1, 3 ) == *( test_data + 1 ) );
    
    sarray_destroy( sa1 );
}

void test_sparse_array()
{
    RUN_TEST( test_sarray_simple_cmp );
    RUN_TEST( test_sarray_remove_node );
    RUN_TEST( test_sarray_remove_last_node );
    RUN_TEST( test_sarray_remove_root );
    RUN_TEST( test_sarray_set_get );
}

int main( int argc, char **argv )
{
    test_llist();
    test_sparse_array();
    return 0;
}